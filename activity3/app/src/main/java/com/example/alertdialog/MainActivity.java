package com.example.alertdialog;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {


    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText Input = findViewById(R.id.et_input);
        Button Show = findViewById(R.id.bt_show);

        Show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String Text =Input.getText().toString();
                if (Text.isEmpty()) {
                    alert("Please Insert Date !!!");
                }else {
                    alert(Text);
                }
            }
        });
    }
    private void alert (String message){
        AlertDialog dlg = new AlertDialog.Builder(MainActivity.this)
                .setTitle("Message")
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    private Dialog dialog;

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialog.dismiss();
                    }
                })
                .create();
        dlg.show();
    }
}