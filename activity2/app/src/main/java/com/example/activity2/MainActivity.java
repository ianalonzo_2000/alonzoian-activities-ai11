package com.example.activity2;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    View screenview;
    Button ChangeColor;
    int[] color;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        color = new int[] {Color.RED, Color.BLACK, Color.WHITE, Color.GREEN, Color.GRAY, Color.YELLOW};
        screenview = findViewById(R.id.rView);
        ChangeColor = (Button) findViewById(R.id.button);

        ChangeColor. setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int aryLength = color.length;

                Random random = new Random();
                int rNum = random.nextInt(aryLength);

                screenview.setBackgroundColor(color[rNum]);
            }
        });
    }
}